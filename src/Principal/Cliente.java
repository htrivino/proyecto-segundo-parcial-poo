package Principal;

import java.io.Serializable;

public class Cliente implements Serializable{
    //private long cedula;
    private String nombres;
    private String apellidos;
    private String direccion;
    private String email;
    
    //public static Cliente clienteActual;

    public Cliente( String nombres,String apellidos, String direccion, String email) {
        //this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.email = email;
    }

    @Override
    public String toString() {
        return nombres+" "+apellidos;
    }

    public String getNombre() {
        return nombres;
    }
    
}
