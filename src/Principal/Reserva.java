/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.io.Serializable;
import java.util.Date;
import recopilardatos.Hotel;

/**
 *
 * @author EMANUEL
 */
public class Reserva implements Serializable {

    private Cliente cliente;
    private Hotel hotel;
    private Date fechaEntrada;
    private Date fechaSalida;
    private int N_habitaciones;
    private Double precioTotal;
    private Double comision;

    public Reserva(Cliente cliente, Hotel hotel, Date fechaEntrada,Date fechaSalida, int N_habitaciones,double precioTotal,double comision) {
        this.cliente = cliente;
        this.hotel = hotel;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida=fechaSalida;
        this.N_habitaciones = N_habitaciones;
        this.precioTotal = precioTotal;
        this.comision = comision;
    }

    public Double getComision() {
        return comision;
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }
     public Date getFechaSalida() {
        return fechaSalida;
    }

    public int getN_habitaciones() {
        return N_habitaciones;
    }

    @Override
    public String toString() {
        return "Reserva{" + "cliente=" + cliente + ", hotel=" + hotel + ", fechaEntrada=" + fechaEntrada + ", fechaSalida=" + fechaSalida + ", N_habitaciones=" + N_habitaciones + ", precioTotal=" + precioTotal + ", comision=" + comision + '}';
    }


    
    
}
