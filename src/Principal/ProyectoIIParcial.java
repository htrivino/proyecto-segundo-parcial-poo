package Principal;

import GUI.VentanaRegistro;
import GUI.VentanaVerReservas;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import recopilardatos.Hotel;
import recopilardatos.Habitacion;
import recopilardatos.Provincia;
import recopilardatos.Catalogo;
import recopilardatos.Servicio;
import recopilardatos.Ciudad;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;
import recopilardatos.Lectura;

public class ProyectoIIParcial extends Application {

    public static List<Servicio> servicios = new ArrayList<>();
    public static List<Catalogo> catalogos = new ArrayList<>();
    public static List<Ciudad> ciudades = new ArrayList<>();
    public static List<Habitacion> habitaciones = new ArrayList<>();
    public static List<Hotel> hoteles = new ArrayList<>();
    public static List<Provincia> provincias = new ArrayList<>();
    
    public static HashMap<String, Cliente> clientes = new HashMap<>();
    
    public static List<Reserva> reservas = new ArrayList<>();

    public static Stage window;
    public static Scene sceneRegistro;
    public static Scene sceneVentanaHotelesI;
    public static Scene sceneVentanaHotelesII;

    //public static Scene sceneInformacion;
    public static Scene sceneVentanaInformacion;
    public static Scene sceneVentanaReserva;
    public static Scene sceneVentanaVerReservas;

    public static Cliente clienteActual;

    public static void main(String[] args) {
        Lectura.leerArchivos();
        deserializar("clientes");
        deserializar("reservas");
        
        //Lectura.DescargaImagenes();
        launch(args);
        
    }

    public static void serializar(String tipo) {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("serializados/"+tipo+".dat"))) {
            if (tipo.equals("clientes")){
                o.writeObject(clientes);
                System.out.println("Se serializó los clientes");
            }
            if(tipo.equals("reservas")){
                o.writeObject(reservas);
                System.out.println("Se serializó la reserva");
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public static void deserializar(String tipo) {
        try (ObjectInputStream i = new ObjectInputStream(new FileInputStream("serializados/"+tipo+".dat"))) {
            if (tipo.equals("clientes")){
            clientes = (HashMap<String, Cliente>) i.readObject();
            System.out.println(clientes);
            System.out.println("Objeto Deserializado-clientes");
            }
            if(tipo.equals("reservas")){
            reservas = (ArrayList<Reserva>) i.readObject();
            System.out.println(reservas);
            System.out.println("Objeto Deserializado-reservas");}
            
        } catch (IOException e) {
            System.err.println("Error en deserializar() " + e);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error en deserializar() " + ex);
        }
    }

    @Override
    public void start(Stage primaryStage) {
        window = primaryStage;
        sceneRegistro = new Scene(new VentanaRegistro().getRoot(), 800, 600);
        window.setScene(sceneRegistro);
        window.setTitle("Bienvenido, a ClickTour!");
        window.show();
    }
}
