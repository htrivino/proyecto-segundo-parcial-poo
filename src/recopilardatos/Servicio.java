package recopilardatos;

/**
 *
 * @author Usuario Final
 */
public class Servicio {
    private int secuencia;
    private int idHotel;
    private int idServicio;
    private String estado;

    public Servicio(int secuencia, int idHotel, int idServicio, String estado) {
        this.secuencia = secuencia;
        this.idHotel = idHotel;
        this.idServicio = idServicio;
        this.estado = estado;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public int getIdHotel() {
        return idHotel;
    }

    @Override
    public String toString() {
        return "Servicio{" + "secuencia=" + secuencia + ", idHotel=" + idHotel + ", idServicio=" + idServicio + ", estado=" + estado + '}';
    }
       
}
