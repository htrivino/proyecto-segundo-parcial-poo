
package recopilardatos;

/**
 *
 * @author Usuario Final
 */
public class Catalogo {
    private int idServicio; 
    private String servicio;

    public Catalogo(int idServicio, String servicio) {
        this.idServicio = idServicio;
        this.servicio = servicio;
    }

    @Override
    public String toString() {
        return "Catalogo{" + "idServicio=" + idServicio + ", servicio=" + servicio + '}';
    }

    public int getIdServicio() {
        return idServicio;
    }

    public String getServicio() {
        return servicio;
    }
    
    
}
