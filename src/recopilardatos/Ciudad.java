
package recopilardatos;

/**
 *
 * @author Usuario Final
 */
public class Ciudad {
    private int idCiudad;
    private int idProvincia;
    private String ciudad;

    public Ciudad(int idCiudad, int idProvincia, String ciudad) {
        this.idCiudad = idCiudad;
        this.idProvincia = idProvincia;
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return ciudad;
    }

    public int getIdProvincia() {
        return idProvincia;
    }

    public int getIdCiudad() {
        return idCiudad;
    }
    
    
    
}
