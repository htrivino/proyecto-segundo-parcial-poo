
package recopilardatos;

/**
 *
 * @author Usuario Final
 */
public class Provincia {
    private int idProvincia;
    private String provincia;
    private String descripcion;
    private String region;
    private String web;

    public Provincia(int idProvincia, String provincia, String descripcion, String region, String web) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.descripcion = descripcion;
        this.region = region;
        this.web = web;
    }

    @Override
    public String toString() {
        return provincia;
    }

    public int getIdProvincia() {
        return idProvincia;
    }
    
    
    
}
