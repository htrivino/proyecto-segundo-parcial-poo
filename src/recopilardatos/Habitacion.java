
package recopilardatos;

/**
 *
 * @author Usuario Final
 */
public class Habitacion {
    private int idHabitacion;
    private int idHotel;
    private String tipoHabitacion;
    private double tarifaSencilla;
    private double tarifaDoble;
    private double tarifaTriple;

    public Habitacion(int idHabitacion, int idHotel, String tipoHabitacion, double tarifaSencilla, double tarifaDoble, double tarifaTriple) {
        this.idHabitacion = idHabitacion;
        this.idHotel = idHotel;
        this.tipoHabitacion = tipoHabitacion;
        this.tarifaSencilla = tarifaSencilla;
        this.tarifaDoble = tarifaDoble;
        this.tarifaTriple = tarifaTriple;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    @Override
    public String toString() {
        return "Tipo de habitacion: " + tipoHabitacion + ", Tarifa Sencilla: " + tarifaSencilla + ", Tarifa Doble: " + tarifaDoble + ", Tarifa Triple: " + tarifaTriple;
    }

    public double getTarifaSencilla() {
        return tarifaSencilla;
    }

    public double getTarifaDoble() {
        return tarifaDoble;
    }

    public double getTarifaTriple() {
        return tarifaTriple;
    }
    
    
}
