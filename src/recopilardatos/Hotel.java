
package recopilardatos;

/**
 *
 * @author Usuario Final
 */
public class Hotel {
    private int idHotel;
    private int iDCiudad;
    private String nombreHotel;
    private String descripcionHotel;
    private String tarjetaHotel;
    private String ubicacionHotel;
    private String direccionHotel;
    private String webHotel;
    private int calificacion;
    private String rutaImagenHotel;
    private double latitud;
    private double longitud;
    private String serviciosNombres = "";
            
    public Hotel(int idHotel, int iDCiudad, String nombreHotel, String descripcionHotel, String tarjetaHotel, String ubicacionHotel, String direccionHotel, String webHotel, int calificacion, String rutaImagenHotel, double latitud, double longitud) {
        this.idHotel = idHotel;
        this.iDCiudad = iDCiudad;
        this.nombreHotel = nombreHotel;
        this.descripcionHotel = descripcionHotel;
        this.tarjetaHotel = tarjetaHotel;
        this.ubicacionHotel = ubicacionHotel;
        this.direccionHotel = direccionHotel;
        this.webHotel = webHotel;
        this.calificacion = calificacion;
        this.rutaImagenHotel = rutaImagenHotel;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    
    public void setServiciosNombres(String servicios) {
        this.serviciosNombres = servicios;
    }

    public String getServiciosNombres() {
        return serviciosNombres;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public String getDescripcionHotel() {
        return descripcionHotel;
    }

    public String getTarjetaHotel() {
        return tarjetaHotel;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public String getRutaImagenHotel() {
        return rutaImagenHotel;
    }

    @Override
    public String toString() {
        return nombreHotel ;
    }

    public int getiDCiudad() {
        return iDCiudad;
    }

    public String getDireccionHotel() {
        return direccionHotel;
    }

    public String getWebHotel() {
        return webHotel;
    }

    public String getNombreHotel() {
        return nombreHotel;
    }

    public void setRutaImagenHotel(String rutaImagenHotel) {
        this.rutaImagenHotel = rutaImagenHotel;
    }
            
             
}
